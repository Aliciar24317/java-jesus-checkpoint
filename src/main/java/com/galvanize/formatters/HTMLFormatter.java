package com.galvanize.formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{
    private String output = "";
    public HTMLFormatter(Booking firstInput) {
        format(firstInput);

    }

    @Override
    public String format(Booking input)
    {
        Booking attempt = new Booking(input.getType(), input.getRoomNumber(), input.getStartTime(), input.getEndTime());
        output = "<dl>\n\t<dt>Type</dt><dd>" + attempt.getType() + "</dd>\n\t<dt>Room Number</dt><dd>" + attempt.getRoomNumber() + "</dd>\n\t<dt>Start Time</dt><dd>" + attempt.getStartTime() + "</dd>\n"
        + "\t<dt>End Time</dt><dd>" + attempt.getEndTime() + "</dd>\n</dl>";
//        System.out.println(attempt.getType() + ", " + attempt.getRoomNumber() + ", " + attempt.getStartTime() + ", "
//        + attempt.getEndTime());
        return output;
    }
    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
