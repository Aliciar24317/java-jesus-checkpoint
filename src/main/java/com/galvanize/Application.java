package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

import java.awt.print.Book;

public class Application {

    public static String getFormatter(Booking firstInput, String secondInput)
    {

        String result = "";
        if(secondInput.equalsIgnoreCase("html"))
        {
            Formatter htmlTest = new HTMLFormatter(firstInput);
            result = ((HTMLFormatter) htmlTest).getOutput();
        } else if (secondInput.equalsIgnoreCase("json"))
        {
            Formatter jsonTest = new JSONFormatter(firstInput);
            result = ((JSONFormatter) jsonTest).getOutput();
        } else  {
            Formatter csvTest = new CSVFormatter(firstInput);
            result = ((CSVFormatter) csvTest).getOutput();
        }
        return result;
    }
    public static void main(String[] args) {
        Booking example = new Booking(Booking.Type.Classroom, "112", "10:30am", "2:00pm");
        System.out.println(getFormatter(example, "JSON"));
    }
}