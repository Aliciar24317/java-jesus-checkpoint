package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{
    private String output = "";

    public JSONFormatter(Booking firstInput) {
        format(firstInput);

    }

    @Override
    public String format(Booking input)
    {
        Booking attempt = new Booking(input.getType(), input.getRoomNumber(), input.getStartTime(), input.getEndTime());
        output = "\"type\": \"" + attempt.getType()+ "\", \n\"roomNumber\": " + attempt.getRoomNumber() + ",\n\"startTime\": \"" + attempt.getStartTime() + "\",\n\"endTime\": \""
                + attempt.getEndTime() + "\"";
//        System.out.println(attempt.getType() + ", " + attempt.getRoomNumber() + ", " + attempt.getStartTime() + ", "
//        + attempt.getEndTime());
        return output;


    }
    public String getOutput() {
        return output;
    }

}
